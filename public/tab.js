const values = [
    [180, 161, 102, 31, 31, 46, 175, 199, 106, 89, 137, 200, 199, 126, 159, 114, 184, 198, 10, 183, 30, 150, 47, 187, 35, 126, 131, 159, 173, 178, 174, 69, 87, 157, 75, 190, 181, 122, 65, 19, 63, 67, 56, 115, 65, 162, 159, 144, 77, 36],
    [195, 31, 102, 148, 119, 28, 190, 197, 145, 190, 84, 91, 62, 101, 72, 178, 81, 92, 119, 103, 183, 57, 122, 47, 170, 12, 19, 193, 196, 199, 47, 43, 85, 152, 158, 151, 42, 26, 112, 44, 163, 186, 50, 121, 152, 49, 169, 136, 198, 98],
    [26, 98, 102, 120, 105, 153, 47, 167, 188, 41, 121, 109, 16, 199, 83, 196, 188, 101, 122, 121, 193, 59, 77, 27, 43, 55, 70, 186, 24, 118, 185, 63, 122, 68, 119, 0, 16, 44, 181, 135, 102, 43, 134, 91, 180, 152, 94, 169, 110, 96]
];

const rule1 = new Map();
rule1.set('order', 'DESC');
rule1.set('action', '<');
rule1.set('value', 100);
rule1.set('color', 'red');
rule1.set('uniq', true);
rule1.set('valueFind', new Map());

const rule2 = new Map();
rule2.set('order', 'ASC');
rule2.set('action', '>');
rule2.set('value', 150);
rule2.set('color', 'blue');
rule2.set('uniq', false);
rule2.set('valueFind', new Map());

const rules = new Map();
rules.set('rule1', rule1);
rules.set('rule2', rule2);

function getRule(ruleName)
{
    if (rules.has(ruleName)) {
        return rules.get(ruleName);
    } else {
        alert('No rule found');
        return null;
    }
}

function applyRule(ruleName)
{
    let rule = getRule(ruleName);
    if (rule !== null) {
        if (rule.has('order')) {
            let maxColumn = defineMaxOfColumn();
            let startColumn = defineStart(rule.get('order'), values);
            let endColumn = defineEnd(rule.get('order'), values);
            let startLine = defineLineStart(maxColumn, rule.get('order'));
            let endLine = defineLineEnd(maxColumn, rule.get('order'));

            if (startColumn < endColumn) {
                rule = forValues(startColumn, endColumn, startLine, endLine, rule);
            } else {
                rule = forValuesReverse(startColumn, endColumn, startLine, endLine, rule);
            }
        }
    }
    rules.set(ruleName, rule);
    generateTab();
}

function applyRuleOnValue(x, y, rule)
{
    if (x in values) {
        if (y in values[x]) {
            let value = values[x][y];
            let valueFind = rule.get('valueFind');

            if (rule.get('uniq') && valueFind.has(value)) {
                return rule;
            }

            switch (rule.get('action')) {
                case '<':
                    if (value < rule.get('value')) {
                        rule = saveValue(rule, valueFind, value, x, y);
                    }
                    break;
                case '>':
                    if (value > rule.get('value')) {
                        rule = saveValue(rule, valueFind, value, x, y);
                    }
                    break;
            }
        }
    }

    return rule;
}

function saveValue(rule, valueFind, value, x, y)
{
    let tabForValue = [];
    if (valueFind.has(value)) {
        tabForValue = valueFind.get(value)
    }

    let result = new Map();
    result.set('x', x);
    result.set('y', y);
    result.set('color', rule.get('color'));
    tabForValue.push(result);
    valueFind.set(value, tabForValue)
    rule.set('valueFind', valueFind);

    return rule;
}

function forValues(startColumn, endColumn, startLine, endLine, rule)
{
    for (let x = startColumn; x <= endColumn; x++) {
        for (let y = startLine; y <= endLine; y++) {
            rule = applyRuleOnValue(x, y, rule)
        }
    }

    return rule;
}

function forValuesReverse(startColumn, endColumn, startLine, endLine, rule)
{
    for (let x = startColumn; x >= endColumn; x--) {
        for (let y = startLine; y >= endLine; y--) {
            rule = applyRuleOnValue(x, y, rule)
        }
    }

    return rule;
}

function defineStart(order, tab)
{
    let start = 0;
    if (order === 'DESC') {
        start = tab.length -1;
    }

    return start;
}

function defineEnd(order, tab)
{
    let end = tab.length -1;
    if (order === 'DESC') {
        end = 0;
    }

    return end;
}

function defineLineStart(maxOfColumn, order)
{
    let start = 0;
    if (order === 'DESC') {
        start = maxOfColumn -1;
    }

    return start;
}

function defineLineEnd(maxOfColumn, order)
{
    let end = maxOfColumn -1;
    if (order === 'DESC') {
        end = 0;
    }

    return end;
}

function defineMaxOfColumn()
{
    let columnMax = 0;
    for (let column of values) {
        if (column.length > columnMax) {
            columnMax = column.length;
        }
    }

    return columnMax;
}

function generateTab()
{
    let html = '<table>';

    let maxColumn = defineMaxOfColumn();

    for(let y = 0; y < maxColumn; y++) {
        html = html + '<tr>';

        for (let x = 0; x < values.length; x++) {
            html = html + '<td style="border: black solid">';
            if (x in values && y in values[x]) {
                html = html + generateCel(x, y);
            }
            html = html + '</td>';
        }
        html = html + '</tr>';
    }

    html = html + '</table>';

    document.getElementById('target').innerHTML = html;
}

function generateCel(x, y)
{
    let html = '';
    for (let usedRule of rules.values()) {
        console.log('usedRule');
        console.log(usedRule);
        let valueFind = usedRule.get('valueFind')
        console.log('valueFind');
        console.log(valueFind);
        if (valueFind.has(values[x][y])) {
            for (let value of valueFind.get(values[x][y])) {
                console.log('value ' + value);
                console.log('x ' + value.get('x'));
                console.log('x ' + x);
                console.log('y ' + y);
                console.log('y ' + value.get('y'));
                if (value.get('x') === x && value.get('y') === y) {
                    html = html + '<span style="color: ' + usedRule.get('color') + '">';
                    html = html + values[x][y];
                    html = html + '</span>';
                }
            }
        }
    }

    if (html === '') {
        html = values[x][y];
    }

    return html;
}